﻿using System;

namespace VowelCountTask
{
    public static class StringHelper
    {
        public static int GetCountOfVowel(string source)
        {
            if (source is null || string.IsNullOrEmpty(source))
            {
                throw new ArgumentException("String cannot be null or empty.");
            }

            int vowelsCount = 0;
            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] == 'a' || source[i] == 'e' || source[i] == 'i' || source[i] == 'o' || source[i] == 'u')
                {
                    vowelsCount++;
                }
            }

            return vowelsCount;
        }
    }
}
